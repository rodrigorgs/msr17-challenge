library(dplyr)
library(data.table)
library(RSQLite)

SENTI_PATH <- '../sentistrength/bli2_out.txt.gz'
senti <- fread(paste0('zcat ', SENTI_PATH))

COMMIT_LOG_PATH <- '../raw-data/commitlog.sqlite'
commitdb <- src_sqlite(COMMIT_LOG_PATH)
commits_keys <- commitdb %>%
  tbl(sql("select project, sha from commits order by project, sha")) %>%
  collect(n = Inf)

sentiment <- cbind(commits_keys, senti)

databasedb <- src_sqlite('../data/database.sqlite')
databasedb$con %>% db_drop_table('sentiment')
copy_to(databasedb, sentiment, temporary = F,
    indexes = list(c("project", "sha")))

commits <- commitdb %>%
  tbl(sql("select * from commits order by project, sha")) %>%
  collect(n = Inf)
databasedb$con %>% db_drop_table('commits')
copy_to(databasedb, commits, temporary = F,
        indexes = list(c("project", "sha")))
