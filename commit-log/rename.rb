#!/usr/bin/env ruby

require 'fileutils'

projects = IO.readlines('projects.txt').map(&:chomp)

projects.each do |proj|
  a, b = proj.split('/')
  begin
    FileUtils.mv b, "#{a}__#{b}"
  rescue
    puts "erro #{b}"
  end
end